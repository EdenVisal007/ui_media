// ignore_for_file: library_private_types_in_public_api, sort_child_properties_last

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:ui_media/constants/mypost_facebook.dart';
import 'package:ui_media/constants/mystory_facebook.dart';
import 'package:ui_media/models/facebook_model.dart';
import 'package:ui_media/page/detail_page.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {

  double? _width;
  bool _iconLike = false;
  bool _iconComment = false;
  bool _iconShare = false;

  @override
  Widget build(BuildContext context) {
    _width = MediaQuery.of(context).size.width;
    return Container(
      color: Colors.grey,
      child: CustomScrollView(
        slivers: [
          _buildSliverAppBar,
          _buildSliverAddPost,
          _buildSliverStory,
          _buildSliverList,
        ],
      ),
    );
  }
  Widget get _buildSliverAppBar{
    return SliverAppBar(
      backgroundColor: Colors.grey[300],
      floating: true,
      title: const Text("Facebook",style: TextStyle(fontFamily: "Billabong",fontSize: 32,color: Colors.blueAccent),),
      actions: [
        IconButton(
          onPressed: (){},
          icon: const Icon(CupertinoIcons.search,color: Colors.black87,),
        ),
        IconButton(
          onPressed: (){},
          icon: const Icon(CupertinoIcons.bolt_horizontal_circle_fill,color: Colors.black87,),
        ),
      ],
    );
  }
  Widget get _buildSliverAddPost{
    return SliverToBoxAdapter(
      child: Container(
        width: _width,
        color: Colors.white70,
        child: Column(
          children: [
            Row(
              children: [
                Container(
                  width: 50,
                  height: 45,
                  margin: const EdgeInsets.only(left: 10),
                  child: Container(
                    height: 40,
                    decoration: const BoxDecoration(
                      shape: BoxShape.circle,
                      image: DecorationImage(
                        image: AssetImage("assets/images/ronaldo1.jpg",),
                        fit: BoxFit.cover,
                        alignment: Alignment(0,-1),
                      )
                    ),
                  ),
                ),
                Container(
                  margin: const EdgeInsets.only(left: 10),
                  padding: const EdgeInsets.all(7),
                  child: const Text("   What's on your mind?                                              ",),
                  decoration: BoxDecoration(
                    border: Border.all(color: Colors.black87),
                    borderRadius: BorderRadius.circular(36),
                  ),
                ),
              ],
            ),
            Container(height: 4,),
            Container(height: 1,color: Colors.grey,),
            SizedBox(
              height: 40,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  _buildLive,
                  Container(width: 1,color: Colors.grey,),
                  _buildPhoto,
                  Container(width: 1,color: Colors.grey,),
                  _buildRoom,
                ],
              ),
            ),
            Container(height: 10,color: Colors.grey,),
          ],
        ),
      ),
    );
  }
  Widget get _buildLive{
    return TextButton.icon(
      onPressed: (){},
      icon: const Icon(Icons.video_call,color: Colors.redAccent,),
      label: const Text("Live"),
    );
  }
  Widget get _buildPhoto{
    return TextButton.icon(
      onPressed: (){},
      icon: const Icon(Icons.photo,color: Colors.green,),
      label: const Text("Photo"),
    );
  }
  Widget get _buildRoom{
    return TextButton.icon(
      onPressed: (){},
      icon: const Icon(Icons.video_settings_rounded,color: Colors.purple,),
      label: const Text("Room"),
    );
  }
  Widget get _buildSliverStory{
    return SliverToBoxAdapter(
      child: Container(
        color: Colors.white70,
        padding: const EdgeInsets.only(left: 8,right: 8,),
        height: 200,
        child: ListView.builder(
          physics: const BouncingScrollPhysics(),
          scrollDirection: Axis.horizontal,
          itemCount: myStoryFacebook.length,
          itemBuilder: (context,index){
            return _buildStoryItem(myStoryFacebook[index]);
          },
        ),
      ),
    );
  }
  Widget _buildStoryItem(FacebookModel item){
    return Container(
      alignment: Alignment.topCenter,
      width: 130,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Stack(
            alignment: Alignment.bottomLeft,
            children: [
              _buildStoryImage(item.image),
              _buildStoryText(item.name)
            ],
          ),
        ],
      ),
    );
  }
  Widget _buildStoryImage(String image){
    return Stack(
      children: [
        SizedBox(
          width: 120,
          height: 180,
          child: Container(
            decoration: BoxDecoration(
              image: DecorationImage(
                image: AssetImage(image),
                fit: BoxFit.cover
              ),
              borderRadius: BorderRadius.circular(16),
              boxShadow: const [
                BoxShadow(
                  spreadRadius: 2,
                  blurRadius: 2,
                  color: Colors.black54
                ),
              ]
            ),
          ),
        ),
        Container(
          padding: const EdgeInsets.only(left: 5,top: 5),
          width: 45,
          height: 45,
          child: Container(
            decoration: const BoxDecoration(
              shape: BoxShape.circle,
              gradient: LinearGradient(
                begin: Alignment.topLeft,
                end: Alignment.bottomRight,
                colors: [
                  Colors.blueAccent,
                  Colors.blue,
                ],
              ),
              boxShadow: [
                BoxShadow(
                  spreadRadius: 1,
                  blurRadius: 1,
                  color: Colors.indigo
                ),
              ]
            ),
          ),
        ),
        Container(
          padding: const EdgeInsets.only(left: 5,top: 5),
          margin: const EdgeInsets.only(left: 2,top: 2),
          height: 40,
          width: 40,
          child: Container(
            decoration: BoxDecoration(
              shape: BoxShape.circle,
              image: DecorationImage(
                image: AssetImage(image),
                fit: BoxFit.cover,
                alignment: const Alignment(0,-1)
              )
            ),
          ),
        ),
      ],
    );
  }
  Widget _buildStoryText(String name){
    return SizedBox(
      width: 100,
      child: Container(
        margin: const EdgeInsets.only(left: 10),
        child: Text(
          name,
          style: const TextStyle(fontSize: 16,color: Colors.white70,fontWeight: FontWeight.bold),
          overflow: TextOverflow.ellipsis,
        ),
      ),
    );
  }
  Widget get _buildSliverList{
    return SliverList(
      delegate: SliverChildBuilderDelegate((context,index){
        return _buildSliverItem(myPostFacebook[index]);
      },
      childCount: myPostFacebook.length),
    );
  }
  Widget _buildSliverItem(FacebookModel item){
    return Card(
      margin: const EdgeInsets.only(top: 10),
      child: Container(
        width: _width,
        height: 640,
        margin: const EdgeInsets.only(top: 10),
        child: Column(
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Row(
                  children: [
                    _buildProfile(item.image),
                    const SizedBox(width: 10,),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          "${item.title}              ",
                          style: const TextStyle(
                            color: Colors.black87,
                            fontSize: 24,
                            fontWeight: FontWeight.bold,
                            fontFamily: "Billabong"
                          ),
                          textAlign: TextAlign.start,
                        ),
                        Row(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: const [
                            Text("Just Now",style: TextStyle(fontSize: 12,),textAlign: TextAlign.start,),
                            SizedBox(width: 5,),
                            Text("."),
                            SizedBox(width: 5,),
                            Icon(Icons.group, size: 20,),
                          ],
                        ),
                      ],
                    ),
                  ],
                ),
                IconButton(onPressed: (){},icon: const Icon(Icons.more_horiz),),
              ],
            ),
            Container(
              width: _width,
              margin: const EdgeInsets.only(left: 15,top: 10),
              child: Text(item.description),
            ),
            const SizedBox(height: 10,),
            InkWell(
              onTap: (){
                Navigator.push(context, MaterialPageRoute(builder: (context) => DetailPage(facebookModel: item,)));
              },
              child: _buildPostImage(item.image)
            ),
            SizedBox(
              width: _width,
              height: 50,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  _buildLike,
                  _buildComment,
                  _buildShare,
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
  Widget _buildProfile(String image){
    return Stack(
      children: [
        Container(
          margin: const EdgeInsets.only(left: 10),
          padding: const EdgeInsets.only(left: 5,top: 5),
          width: 55,
          height: 55,
          child: Container(
            decoration: const BoxDecoration(
              shape: BoxShape.circle,
              gradient: LinearGradient(
                begin: Alignment.topLeft,
                end: Alignment.bottomRight,
                colors: [
                  Colors.grey,
                  Colors.blueGrey,
                ],
              ),
              boxShadow: [
                BoxShadow(
                  spreadRadius: 1,
                  blurRadius: 1,
                  color: Colors.indigo
                ),
              ],
            ),
          ),
        ),
        Container(
          padding: const EdgeInsets.only(left: 5,top: 5),
          margin: const EdgeInsets.only(left: 12,top: 2),
          height: 50,
          width: 50,
          child: Container(
            decoration: BoxDecoration(
              shape: BoxShape.circle,
              image: DecorationImage(
                image: AssetImage(image),
                fit: BoxFit.cover,
                alignment: const Alignment(0,-1)
              ),
            ),
          ),
        ),
      ],
    );
  }
  Widget _buildPostImage(String image){
    return SizedBox(
      width: MediaQuery.of(context).size.width,
      height: 450,
      child: Image.asset(image,fit: BoxFit.cover,alignment: const Alignment(0,-1),),
    );
  }
  Widget get _buildLike{
    return TextButton.icon(
      onPressed: (){
        setState(() {
          _iconLike = !_iconLike;
        });
      },
      icon: _iconLike ?
        const Icon(Icons.thumb_up,color: Colors.blue,) :
        const Icon(Icons.thumb_up_alt_outlined, color: Colors.grey,),
      label: Text("Like",style: TextStyle(color: _iconLike ? Colors.blue : Colors.black),),
    );
  }
  Widget get _buildComment{
    return TextButton.icon(
      onPressed: (){
        setState(() {
          _iconComment = !_iconComment;
        });
      },
      icon: _iconComment ?
        const Icon(CupertinoIcons.chat_bubble_fill,color: Colors.blue,) :
        const Icon(CupertinoIcons.chat_bubble, color: Colors.grey,),
      label: Text("Comment",style: TextStyle(color: _iconComment ? Colors.blue : Colors.black),),
    );
  }
  Widget get _buildShare{
    return TextButton.icon(
      onPressed: (){
        setState(() {
          _iconShare = !_iconShare;
        });
      },
      icon: _iconShare ?
        const Icon(CupertinoIcons.arrowshape_turn_up_right_fill,color: Colors.blue,) :
        const Icon(CupertinoIcons.arrowshape_turn_up_right,color: Colors.grey,),
      label: Text("Share",style: TextStyle(color: _iconShare ? Colors.blue : Colors.black),),
    );
  }
}
