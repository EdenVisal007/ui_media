// ignore_for_file: prefer_const_constructors_in_immutables, use_key_in_widget_constructors, library_private_types_in_public_api

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:ui_media/models/facebook_model.dart';

class DetailPage extends StatefulWidget {

  final FacebookModel facebookModel;
  DetailPage({required this.facebookModel});

  @override
  _DetailPageState createState() => _DetailPageState();
}

class _DetailPageState extends State<DetailPage> {

  bool _iconLike = false;
  bool _iconComment = false;
  bool _iconShare = false;
  bool _showData = false;

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: TextButton(
        onPressed: (){
          setState(() {
            _showData = !_showData;
          });
        },
        child: Stack(
          children: [
            Container(
              alignment: Alignment.center,
              child: Container(
                decoration: BoxDecoration(
                  gradient: LinearGradient(
                    colors: [
                      Colors.orange.withOpacity(0.6),
                      Colors.deepOrange.withOpacity(0.8)
                    ]
                  ),
                  image: DecorationImage(
                    image: AssetImage(widget.facebookModel.image),
                    fit: BoxFit.cover,
                  )
                ),
              ),
            ),
            _showData ? Container() : Container(
              color: Colors.black38,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Container(
                    alignment: Alignment.centerRight,
                    margin: const EdgeInsets.only(top: 20,right: 10),
                    child: const Icon(Icons.more_vert_outlined,color: Colors.white,)
                  ),
                  Column(
                    children: [
                      Container(
                        padding: const EdgeInsets.only(top: 10,left: 10,bottom: 10),
                        child: Text(
                          widget.facebookModel.description,
                          style: const TextStyle(fontSize: 14,color: Colors.white, decoration: TextDecoration.none),
                        ),
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: [
                          _buildLike,
                          _buildComment,
                          _buildShare,
                        ],
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
  Widget get _buildLike{
    return TextButton.icon(
      onPressed: (){
        setState(() {
          _iconLike = !_iconLike;
        });
      },
      icon: _iconLike ?
      const Icon(Icons.thumb_up,color: Colors.blue,) :
      const Icon(Icons.thumb_up_alt_outlined, color: Colors.white,),
      label: Text("Like",style: TextStyle(color: _iconLike ? Colors.blue : Colors.white),),
    );
  }
  Widget get _buildComment{
    return TextButton.icon(
      onPressed: (){
        setState(() {
          _iconComment = !_iconComment;
        });
      },
      icon: _iconComment ?
      const Icon(CupertinoIcons.chat_bubble_fill,color: Colors.blue,) :
      const Icon(CupertinoIcons.chat_bubble, color: Colors.white,),
      label: Text("Comment",style: TextStyle(color: _iconComment ? Colors.blue : Colors.white),),
    );
  }
  Widget get _buildShare{
    return TextButton.icon(
      onPressed: (){
        setState(() {
          _iconShare = !_iconShare;
        });
      },
      icon: _iconShare ?
      const Icon(CupertinoIcons.arrowshape_turn_up_right_fill,color: Colors.blue,) :
      const Icon(CupertinoIcons.arrowshape_turn_up_right,color: Colors.white,),
      label: Text("Share",style: TextStyle(color: _iconShare ? Colors.blue : Colors.white),),
    );
  }
}
