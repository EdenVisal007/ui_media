import 'package:flutter/material.dart';
import 'group_page.dart';
import 'home_page.dart';
import 'menu_page.dart';
import 'notification_page.dart';
import 'video_page.dart';

class MainPage extends StatefulWidget {
  const MainPage({Key? key}) : super(key: key);

  @override
  _MainPageState createState() => _MainPageState();
}

class _MainPageState extends State<MainPage> {

  late List<Widget> _listFacebook;
  final HomePage _homePage = const HomePage();
  final GroupPage _groupPage = const GroupPage();
  final VideoPage _videoPage = const VideoPage();
  final NotificationPage _notificationPage = const NotificationPage();
  final MenuPage _menuPage = const MenuPage();

  @override
  void initState() {
    super.initState();
    _listFacebook = [_homePage,_groupPage,_videoPage,_notificationPage,_menuPage];
  }

  final PageController _pageController = PageController();
  int _currentIndex = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: _buildBody,
      bottomNavigationBar: _buildBottomNavigationBar,
    );
  }
  Widget get _buildBody{
    return SafeArea(
      child: PageView(
        controller: _pageController,
        onPageChanged: (index){
          setState(() {
            _currentIndex = index;
          });
          _pageController.jumpToPage(_currentIndex);
        },
        children: _listFacebook
      ),
    );
  }
  Widget get _buildBottomNavigationBar{
    return BottomNavigationBar(
      selectedItemColor: Colors.blueAccent,
      unselectedItemColor: Colors.black54,
      currentIndex: _currentIndex,
      onTap: (index){
        setState(() {
          _currentIndex = index;
        });
        _pageController.jumpToPage(_currentIndex);
      },
      type: BottomNavigationBarType.fixed,
      items: const <BottomNavigationBarItem> [
        BottomNavigationBarItem(
          icon: Icon(Icons.home),
          label: "Home"
        ),
        BottomNavigationBarItem(
          icon: Icon(Icons.group_work_sharp),
          label: "Group"
        ),
        BottomNavigationBarItem(
          icon: Icon(Icons.video_settings_rounded),
          label: "Watch"
        ),
        BottomNavigationBarItem(
          icon: Icon(Icons.notification_important_sharp),
          label: "Notification"
        ),
        BottomNavigationBarItem(
          icon: Icon(Icons.menu),
          label: "Menu"
        ),
      ],
    );
  }
}
