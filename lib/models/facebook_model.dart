class FacebookModel{
  String name;
  String title;
  String description;
  String image;
  FacebookModel({
    this.name = "no name",
    this.title = "no title",
    this.description = "no description",
    this.image = "no image"
  });
}