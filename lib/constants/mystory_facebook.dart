import 'package:ui_media/models/facebook_model.dart';

List<FacebookModel> myStoryFacebook = [
  FacebookModel(
      name: "Ronaldo",
      title: "Cristiano Ronaldo",
      description: "Ronaldo was born on February 5, 1985, in Funchal, Madeira, Portugal, a small island off the western coast of the country. Ronaldo is the youngest of four children born to Maria Dolores dos Santos and José Dinis Aveiro. He was named after Ronald Reagan, one of his father's favorite actors.",
      image: "assets/images/ronaldo.jpg"
  ),
  FacebookModel(
      name: "Messi",
      title: "Lionel Messi",
      description: "Lionel Messi, in full Lionel Andrés Messi, also called Leo Messi, (born June 24, 1987, Rosario, Argentina), Argentine-born football (soccer) player who was named Fédération Internationale de Football Association ",
      image: "assets/images/messi.jpg"
  ),
  FacebookModel(
      name: "Neymar",
      title: "Neymar da Silva Santos Júnior",
      description: "Neymar da Silva Santos Junior, known as Neymar or Neymar Jr. (born 5 February 1992) is a professional Brazilian footballer that can play as a left or right Winger or as a Striker. ... He is the most expensive footballer of all time with a transfer fee of 222 million euros from FC Barcelona to PSG in August 2017.",
      image: "assets/images/neymar.jpg"
  ),
  FacebookModel(
      name: "Mbappe",
      title: "Kylian Mbappé Lottin",
      description: "Mbappé was born in Paris and was raised in Bondy, Seine-Saint-Denis, a commune 10.9 km (6.8 mi) from the centre of Paris. His father, Wilfried, is from Cameroon, and, as well as being Mbappé's agent, is a football coach, while his mother, Fayza Lamari, is of Algerian Kabyle origin and is a former handball player.",
      image: "assets/images/mbappe.jpg"
  ),
  FacebookModel(
      name: "Ronaldo",
      title: "Cristiano Ronaldo",
      description: "Ronaldo was born on February 5, 1985, in Funchal, Madeira, Portugal, a small island off the western coast of the country. Ronaldo is the youngest of four children born to Maria Dolores dos Santos and José Dinis Aveiro. He was named after Ronald Reagan, one of his father's favorite actors.",
      image: "assets/images/ronaldo.jpg"
  ),
  FacebookModel(
      name: "Messi",
      title: "Lionel Messi",
      description: "Lionel Messi, in full Lionel Andrés Messi, also called Leo Messi, (born June 24, 1987, Rosario, Argentina), Argentine-born football (soccer) player who was named Fédération Internationale de Football Association ",
      image: "assets/images/messi.jpg"
  ),
  FacebookModel(
      name: "Neymar",
      title: "Neymar da Silva Santos Júnior",
      description: "Neymar da Silva Santos Junior, known as Neymar or Neymar Jr. (born 5 February 1992) is a professional Brazilian footballer that can play as a left or right Winger or as a Striker. ... He is the most expensive footballer of all time with a transfer fee of 222 million euros from FC Barcelona to PSG in August 2017.",
      image: "assets/images/neymar.jpg"
  ),
  FacebookModel(
      name: "Mbappe",
      title: "Kylian Mbappé Lottin",
      description: "Mbappé was born in Paris and was raised in Bondy, Seine-Saint-Denis, a commune 10.9 km (6.8 mi) from the centre of Paris. His father, Wilfried, is from Cameroon, and, as well as being Mbappé's agent, is a football coach, while his mother, Fayza Lamari, is of Algerian Kabyle origin and is a former handball player.",
      image: "assets/images/mbappe.jpg"
  ),
  FacebookModel(
      name: "Ronaldo",
      title: "Cristiano Ronaldo",
      description: "Ronaldo was born on February 5, 1985, in Funchal, Madeira, Portugal, a small island off the western coast of the country. Ronaldo is the youngest of four children born to Maria Dolores dos Santos and José Dinis Aveiro. He was named after Ronald Reagan, one of his father's favorite actors.",
      image: "assets/images/ronaldo.jpg"
  ),
  FacebookModel(
      name: "Messi",
      title: "Lionel Messi",
      description: "Lionel Messi, in full Lionel Andrés Messi, also called Leo Messi, (born June 24, 1987, Rosario, Argentina), Argentine-born football (soccer) player who was named Fédération Internationale de Football Association ",
      image: "assets/images/messi.jpg"
  ),
  FacebookModel(
      name: "Neymar",
      title: "Neymar da Silva Santos Júnior",
      description: "Neymar da Silva Santos Junior, known as Neymar or Neymar Jr. (born 5 February 1992) is a professional Brazilian footballer that can play as a left or right Winger or as a Striker. ... He is the most expensive footballer of all time with a transfer fee of 222 million euros from FC Barcelona to PSG in August 2017.",
      image: "assets/images/neymar.jpg"
  ),
  FacebookModel(
      name: "Mbappe",
      title: "Kylian Mbappé Lottin",
      description: "Mbappé was born in Paris and was raised in Bondy, Seine-Saint-Denis, a commune 10.9 km (6.8 mi) from the centre of Paris. His father, Wilfried, is from Cameroon, and, as well as being Mbappé's agent, is a football coach, while his mother, Fayza Lamari, is of Algerian Kabyle origin and is a former handball player.",
      image: "assets/images/mbappe.jpg"
  ),
  FacebookModel(
      name: "Ronaldo",
      title: "Cristiano Ronaldo",
      description: "Ronaldo was born on February 5, 1985, in Funchal, Madeira, Portugal, a small island off the western coast of the country. Ronaldo is the youngest of four children born to Maria Dolores dos Santos and José Dinis Aveiro. He was named after Ronald Reagan, one of his father's favorite actors.",
      image: "assets/images/ronaldo.jpg"
  ),
  FacebookModel(
      name: "Messi",
      title: "Lionel Messi",
      description: "Lionel Messi, in full Lionel Andrés Messi, also called Leo Messi, (born June 24, 1987, Rosario, Argentina), Argentine-born football (soccer) player who was named Fédération Internationale de Football Association ",
      image: "assets/images/messi.jpg"
  ),
  FacebookModel(
      name: "Neymar",
      title: "Neymar da Silva Santos Júnior",
      description: "Neymar da Silva Santos Junior, known as Neymar or Neymar Jr. (born 5 February 1992) is a professional Brazilian footballer that can play as a left or right Winger or as a Striker. ... He is the most expensive footballer of all time with a transfer fee of 222 million euros from FC Barcelona to PSG in August 2017.",
      image: "assets/images/neymar.jpg"
  ),
  FacebookModel(
      name: "Mbappe",
      title: "Kylian Mbappé Lottin",
      description: "Mbappé was born in Paris and was raised in Bondy, Seine-Saint-Denis, a commune 10.9 km (6.8 mi) from the centre of Paris. His father, Wilfried, is from Cameroon, and, as well as being Mbappé's agent, is a football coach, while his mother, Fayza Lamari, is of Algerian Kabyle origin and is a former handball player.",
      image: "assets/images/mbappe.jpg"
  ),
  FacebookModel(
      name: "Ronaldo",
      title: "Cristiano Ronaldo",
      description: "Ronaldo was born on February 5, 1985, in Funchal, Madeira, Portugal, a small island off the western coast of the country. Ronaldo is the youngest of four children born to Maria Dolores dos Santos and José Dinis Aveiro. He was named after Ronald Reagan, one of his father's favorite actors.",
      image: "assets/images/ronaldo.jpg"
  ),
  FacebookModel(
      name: "Messi",
      title: "Lionel Messi",
      description: "Lionel Messi, in full Lionel Andrés Messi, also called Leo Messi, (born June 24, 1987, Rosario, Argentina), Argentine-born football (soccer) player who was named Fédération Internationale de Football Association ",
      image: "assets/images/messi.jpg"
  ),
  FacebookModel(
      name: "Neymar",
      title: "Neymar da Silva Santos Júnior",
      description: "Neymar da Silva Santos Junior, known as Neymar or Neymar Jr. (born 5 February 1992) is a professional Brazilian footballer that can play as a left or right Winger or as a Striker. ... He is the most expensive footballer of all time with a transfer fee of 222 million euros from FC Barcelona to PSG in August 2017.",
      image: "assets/images/neymar.jpg"
  ),
  FacebookModel(
      name: "Mbappe",
      title: "Kylian Mbappé Lottin",
      description: "Mbappé was born in Paris and was raised in Bondy, Seine-Saint-Denis, a commune 10.9 km (6.8 mi) from the centre of Paris. His father, Wilfried, is from Cameroon, and, as well as being Mbappé's agent, is a football coach, while his mother, Fayza Lamari, is of Algerian Kabyle origin and is a former handball player.",
      image: "assets/images/mbappe.jpg"
  ),
];